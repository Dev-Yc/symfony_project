<?php

namespace App\Entity;

use App\Repository\ExercicesRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ExercicesRepository::class)
 */
class Exercices
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Nom;

    /**
     * @ORM\ManyToOne(targetEntity=Categorie::class, inversedBy="Exercices")
     * @ORM\JoinColumn(nullable=false)
     */
    private $Partie_Corps;

    /**
     * @ORM\ManyToMany(targetEntity=Seance::class, mappedBy="Exercice_id")
     */
    private $exercices;

    public function __construct()
    {
        $this->exercices = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->Nom;
    }

    public function setNom(string $Nom): self
    {
        $this->Nom = $Nom;

        return $this;
    }

    public function getPartieCorps(): ?Categorie
    {
        return $this->Partie_Corps;
    }

    public function setPartieCorps(?Categorie $Partie_Corps): self
    {
        $this->Partie_Corps = $Partie_Corps;

        return $this;
    }

    /**
     * @return Collection<int, Seance>
     */
    public function getExercices(): Collection
    {
        return $this->exercices;
    }

    public function addExercice(Seance $exercice): self
    {
        if (!$this->exercices->contains($exercice)) {
            $this->exercices[] = $exercice;
            $exercice->addExerciceId($this);
        }

        return $this;
    }

    public function removeExercice(Seance $exercice): self
    {
        if ($this->exercices->removeElement($exercice)) {
            $exercice->removeExerciceId($this);
        }

        return $this;
    }
}
