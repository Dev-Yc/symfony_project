<?php

namespace App\Entity;

use App\Repository\SeanceRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=SeanceRepository::class)
 */
class Seance
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Nom;

    /**
     * @ORM\ManyToMany(targetEntity=Exercices::class, inversedBy="exercices")
     */
    private $Exercice_id;

    public function __construct()
    {
        $this->Exercice_id = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->Nom;
    }

    public function setNom(string $Nom): self
    {
        $this->Nom = $Nom;

        return $this;
    }

    /**
     * @return Collection<int, Exercices>
     */
    public function getExerciceId(): Collection
    {
        return $this->Exercice_id;
    }

    public function addExerciceId(Exercices $exerciceId): self
    {
        if (!$this->Exercice_id->contains($exerciceId)) {
            $this->Exercice_id[] = $exerciceId;
        }

        return $this;
    }

    public function removeExerciceId(Exercices $exerciceId): self
    {
        $this->Exercice_id->removeElement($exerciceId);

        return $this;
    }
}
