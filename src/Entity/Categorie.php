<?php

namespace App\Entity;

use App\Repository\CategorieRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=CategorieRepository::class)
 */
class Categorie
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Nom;

    /**
     * @ORM\OneToMany(targetEntity=Exercices::class, mappedBy="Partie_Corps")
     */
    private $Exercices;

    public function __construct()
    {
        $this->Exercices = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->Nom;
    }

    public function setNom(string $Nom): self
    {
        $this->Nom = $Nom;

        return $this;
    }

    /**
     * @return Collection<int, Exercices>
     */
    public function getExercices(): Collection
    {
        return $this->Exercices;
    }

    public function addExercice(Exercices $exercice): self
    {
        if (!$this->Exercices->contains($exercice)) {
            $this->Exercices[] = $exercice;
            $exercice->setPartieCorps($this);
        }

        return $this;
    }

    public function removeExercice(Exercices $exercice): self
    {
        if ($this->Exercices->removeElement($exercice)) {
            // set the owning side to null (unless already changed)
            if ($exercice->getPartieCorps() === $this) {
                $exercice->setPartieCorps(null);
            }
        }

        return $this;
    }
}
